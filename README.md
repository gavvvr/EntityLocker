EntityLocker
===

Utility for synchronised execution of protected code dealing with entities.  

[![pipeline status](https://gitlab.com/gavvvr/EntityLocker/badges/master/pipeline.svg)](https://gitlab.com/gavvvr/EntityLocker/-/commits/master)
[![sonar report](https://sonarcloud.io/api/project_badges/measure?project=gavvvr_EntityLocker&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=gavvvr_EntityLocker)
[![javadoc](https://img.shields.io/badge/javadoc-html-brightgreen)](http://gavvvr.gitlab.io/EntityLocker)


## Features

- locking on entity id(s) or global lock to execute protected code
- arbitrary type of entity id
- locks are reentrant
- possibility to specify timeout for locking
