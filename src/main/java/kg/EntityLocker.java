package kg;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;

/**
 * An interface for synchronized execution of protected code.
 * You can either use a global lock or use an entity identifier of type {@code <T>} to lock on.
 * For any given entity, {@link EntityLocker} guarantees that at most one thread executes protected code on that entity.
 * The interface provides methods:
 * <ul>
 *     <li>having return value or returning nothing</li>
 *     <li>locking on particular entity ID or globally</li>
 *     <li>locking indefinitely or with a timeout</li>
 * </ul>
 *
 * @param <T> type of Entity identifiers. The {@code equals()} and {@code hashcode()} methods of identifier type
 *            must be properly implemented
 */
public interface EntityLocker<T> {
    /**
     * Exclusively executes provided code by locking on entity id
     *
     * @param entityId      Entity id
     * @param protectedCode the code protected from concurrent execution
     * @throws NullPointerException if the specified id is null
     */
    void executeLocked(T entityId, Runnable protectedCode);

    /**
     * Exclusively executes provided code (and returns a value) by locking on entity id
     *
     * @param entityId      Entity id
     * @param protectedCode the code protected from concurrent execution
     * @param <R>           the type of return value
     * @return return value of protected code
     * @throws NullPointerException if the specified id is null
     */
    <R> R executeLocked(T entityId, Supplier<R> protectedCode);

    /**
     * Exclusively executes the code by not allowing any other protected code to be executed at the same time
     *
     * @param protectedCode the code protected from concurrent execution
     */
    void executeExclusively(Runnable protectedCode);

    /**
     * Exclusively executes the code (and returns a value)
     * by not allowing any other protected code to be executed at the same time
     *
     * @param protectedCode the code protected from concurrent execution
     * @param <R>           return value
     * @return return value of protected code
     */
    <R> R executeExclusively(Supplier<R> protectedCode);

    /**
     * Tries to start an exclusive execution of protected code within given amount of time, else throws an exception
     *
     * @param entityId      Entity id
     * @param protectedCode the code protected from concurrent execution
     * @param timeout       the maximum time to wait for the lock
     * @param timeUnit      the time unit of the {@code timeout} argument
     * @throws InterruptedException if the current thread is interrupted
     * @throws TimeoutException     if timed out while waiting
     * @throws NullPointerException if the specified id is null
     */
    void tryExecuteLocked(T entityId, Runnable protectedCode, long timeout, TimeUnit timeUnit) throws InterruptedException, TimeoutException;

    /**
     * Tries to start an exclusive execution of protected code within given amount of time
     * (and returns value), else throws an exception
     *
     * @param entityId      Entity id
     * @param protectedCode the code protected from concurrent execution
     * @param timeout       the maximum time to wait for the lock
     * @param timeUnit      the time unit of the {@code timeout} argument
     * @param <R>           the type of return value
     * @return return value of protected code
     * @throws InterruptedException if the current thread is interrupted
     * @throws TimeoutException     if timed out while waiting
     * @throws NullPointerException if the specified id is null
     */
    <R> R tryExecuteLocked(T entityId, Supplier<R> protectedCode, long timeout, TimeUnit timeUnit) throws InterruptedException, TimeoutException;

    /**
     * Tries to start an exclusive execution of protected code within given amount of time, else throws an exception
     *
     * @param protectedCode the code protected from concurrent execution
     * @param timeout       the maximum time to wait for the lock
     * @param timeUnit      the time unit of the {@code timeout} argument
     * @throws InterruptedException if the current thread is interrupted
     * @throws TimeoutException     if timed out while waiting
     */
    void tryExecuteExclusively(Runnable protectedCode, long timeout, TimeUnit timeUnit) throws InterruptedException, TimeoutException;

    /**
     * Tries to start an exclusive execution of protected code within given amount of time
     * (and returns value), else throws an exception
     *
     * @param protectedCode the code protected from concurrent execution
     * @param timeout       the maximum time to wait for the lock
     * @param timeUnit      the time unit of the {@code timeout} argument
     * @param <R>           the type of return value
     * @return return value of protected code
     * @throws InterruptedException if the current thread is interrupted
     * @throws TimeoutException     if timed out while waiting
     */
    <R> R tryExecuteExclusively(Supplier<R> protectedCode, long timeout, TimeUnit timeUnit) throws InterruptedException, TimeoutException;
}
