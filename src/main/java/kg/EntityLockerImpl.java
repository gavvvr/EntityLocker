package kg;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * An implementation of {@link EntityLocker } for synchronized execution of protected code dealing with entities.
 * For any given entity it is guaranteed that at most one thread executes protected code on that entity.
 * It's not allowed to try to acquire global lock while holding at least one entity lock (and vice versa).
 * But the user of the class must take care of himself to not allow a deadlock by locking multiple entities at once.
 *
 * @param <T> type of Entity identifiers. The {@code equals()} and {@code hashcode()} methods of identifier type
 *            must be properly implemented
 */
public class EntityLockerImpl<T> implements EntityLocker<T> {

    private final boolean fairLocks;

    private final ReentrantReadWriteLock rwLock;
    private final Lock sharedLock;
    private final Lock globalLock;

    private final ConcurrentMap<T, CountingReentrantLock> locksByEntityIds = new ConcurrentHashMap<>();

    /**
     * @param fair fairness of underlying locks
     */
    public EntityLockerImpl(boolean fair) {
        this.fairLocks = fair;

        this.rwLock = new ReentrantReadWriteLock(this.fairLocks);
        this.globalLock = rwLock.writeLock();
        this.sharedLock = rwLock.readLock();
    }

    /**
     * {@inheritDoc}
     *
     * @throws IllegalStateException if current thread already holds global lock
     */
    @Override
    public void executeLocked(T entityId, Runnable protectedCode) {
        ensureNotNull(entityId);
        ensureNotHoldingGlobalLock();

        CountingReentrantLock entityLock = synchronizedTakeEntityLock(entityId);
        try {
            entityLock.lock();
            try {
                sharedLock.lock();
                try {
                    protectedCode.run();
                } finally {
                    sharedLock.unlock();
                }
            } finally {
                entityLock.unlock();
            }
        } finally {
            synchronizedReturnEntityLock(entityId, entityLock);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @throws IllegalStateException if current thread already holds global lock
     */
    @Override
    public <R> R executeLocked(T entityId, Supplier<R> protectedCode) {
        ensureNotNull(entityId);
        ensureNotHoldingGlobalLock();

        CountingReentrantLock entityLock = synchronizedTakeEntityLock(entityId);
        try {
            entityLock.lock();
            try {
                sharedLock.lock();
                try {
                    return protectedCode.get();
                } finally {
                    sharedLock.unlock();
                }
            } finally {
                entityLock.unlock();
            }
        } finally {
            synchronizedReturnEntityLock(entityId, entityLock);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @throws IllegalStateException if current thread already holds any entity lock
     */
    @Override
    public void executeExclusively(Runnable protectedCode) {
        ensureNoSharedLockAcquiredByCurrentThread();
        globalLock.lock();
        try {
            protectedCode.run();
        } finally {
            globalLock.unlock();
        }
    }

    /**
     * {@inheritDoc}
     *
     * @throws IllegalStateException if current thread already holds any entity lock
     */
    @Override
    public <R> R executeExclusively(Supplier<R> protectedCode) {
        ensureNoSharedLockAcquiredByCurrentThread();
        globalLock.lock();
        try {
            return protectedCode.get();
        } finally {
            globalLock.unlock();
        }
    }

    private void ensureNotHoldingGlobalLock() {
        if (rwLock.getWriteHoldCount() > 0) {
            throw new IllegalStateException("It's not allowed to acquire entity lock while holding global lock");
        }
    }

    private void ensureNoSharedLockAcquiredByCurrentThread() {
        if (rwLock.getReadHoldCount() > 0) {
            throw new IllegalStateException("It's not allowed to acquire global lock while holding entity lock");
        }
    }

    /**
     * {@inheritDoc}
     *
     * @throws IllegalStateException if current thread already holds global lock
     */
    @Override
    public void tryExecuteLocked(T entityId, Runnable protectedCode, long timeout, TimeUnit timeUnit) throws InterruptedException, TimeoutException {
        ensureNotNull(entityId);
        ensureNotHoldingGlobalLock();

        doTryExecuteLocked(entityId, protectedCode, timeout, timeUnit);
    }

    @SuppressWarnings("squid:S3626") // FP https://jira.sonarsource.com/browse/SONARJAVA-3058
    private void doTryExecuteLocked(T entityId, Runnable protectedCode, long timeout, TimeUnit timeUnit) throws InterruptedException, TimeoutException {
        long timeoutMillis = timeUnit.toMillis(timeout);
        long timestampBeforeEntityLock = System.currentTimeMillis();
        CountingReentrantLock entityLock = synchronizedTakeEntityLock(entityId);
        try {
            if (entityLock.tryLock(timeoutMillis, MILLISECONDS)) {
                try {
                    long timestampAfterEntityLock = System.currentTimeMillis();
                    if (sharedLock.tryLock(timeoutMillis - timestampAfterEntityLock + timestampBeforeEntityLock, MILLISECONDS)) {
                        try {
                            protectedCode.run();
                            return;
                        } finally {
                            sharedLock.unlock();
                        }
                    }
                } finally {
                    entityLock.unlock();
                }
            }
        } finally {
            synchronizedReturnEntityLock(entityId, entityLock);
        }

        throw new TimeoutException();
    }

    /**
     * {@inheritDoc}
     *
     * @throws IllegalStateException if current thread already holds global lock
     */
    @Override
    public <R> R tryExecuteLocked(T entityId, Supplier<R> protectedCode, long timeout, TimeUnit timeUnit) throws InterruptedException, TimeoutException {
        ensureNotNull(entityId);
        ensureNotHoldingGlobalLock();

        return doTryExecuteLocked(entityId, protectedCode, timeout, timeUnit);
    }

    private <R> R doTryExecuteLocked(T entityId, Supplier<R> protectedCode, long timeout, TimeUnit timeUnit) throws InterruptedException, TimeoutException {
        long timeoutMillis = timeUnit.toMillis(timeout);
        long timestampBeforeEntityLock = System.currentTimeMillis();
        CountingReentrantLock entityLock = synchronizedTakeEntityLock(entityId);
        try {
            if (entityLock.tryLock(timeoutMillis, MILLISECONDS)) {
                try {
                    long timestampAfterEntityLock = System.currentTimeMillis();
                    if (sharedLock.tryLock(timeoutMillis - timestampAfterEntityLock + timestampBeforeEntityLock, MILLISECONDS)) {
                        try {
                            return protectedCode.get();
                        } finally {
                            sharedLock.unlock();
                        }
                    }
                } finally {
                    entityLock.unlock();
                }
            }
        } finally {
            synchronizedReturnEntityLock(entityId, entityLock);
        }

        throw new TimeoutException();
    }

    /**
     * {@inheritDoc}
     *
     * @throws IllegalStateException if current thread already holds any entity lock
     */
    @Override
    @SuppressWarnings("squid:S3626") // FP https://jira.sonarsource.com/browse/SONARJAVA-3058
    public void tryExecuteExclusively(Runnable protectedCode, long timeout, TimeUnit timeUnit) throws InterruptedException, TimeoutException {
        ensureNoSharedLockAcquiredByCurrentThread();

        if (globalLock.tryLock(timeout, timeUnit)) {
            try {
                protectedCode.run();
                return;
            } finally {
                globalLock.unlock();
            }
        }

        throw new TimeoutException();
    }

    /**
     * {@inheritDoc}
     *
     * @throws IllegalStateException if current thread already holds any entity lock
     */
    @Override
    public <R> R tryExecuteExclusively(Supplier<R> protectedCode, long timeout, TimeUnit timeUnit) throws InterruptedException, TimeoutException {
        ensureNoSharedLockAcquiredByCurrentThread();

        if (globalLock.tryLock(timeout, timeUnit)) {
            try {
                return protectedCode.get();
            } finally {
                globalLock.unlock();
            }
        }

        throw new TimeoutException();
    }

    private void ensureNotNull(T id) {
        Objects.requireNonNull(id, "Entity id can not be null");
    }

    private CountingReentrantLock synchronizedTakeEntityLock(T id) {
        return locksByEntityIds.compute(id, (k, l) -> {
            CountingReentrantLock returnLock;
            returnLock = l == null ? new CountingReentrantLock(fairLocks) : l;

            returnLock.takeForUsage();
            return returnLock;
        });
    }

    private void synchronizedReturnEntityLock(T entityId, CountingReentrantLock lock) {
        locksByEntityIds.compute(entityId, (i, v) -> lock.returnBack() == 0 ? null : lock);
    }

    private static class CountingReentrantLock extends ReentrantLock {
        /**
         * This field needs to be volatile to ensure it's updated value is visible for threads
         * taking/releasing the lock.
         * There is no additional atomicity enforcement on increment/decrement
         * because atomicity is guaranteed by locking on {@link EntityLockerImpl}'s  internal concurrent map
         */
        private volatile long usages = 0;

        CountingReentrantLock(boolean fair) {
            super(fair);
        }

        @SuppressWarnings({"NonAtomicOperationOnVolatileField", "squid:S3078"})
        public void takeForUsage() {
            usages++;
        }

        @SuppressWarnings({"NonAtomicOperationOnVolatileField", "squid:S3078"})
        public long returnBack() {
            return --usages;
        }
    }
}
