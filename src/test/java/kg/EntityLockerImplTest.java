package kg;

import net.jodah.concurrentunit.Waiter;
import org.awaitility.core.ThrowingRunnable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeoutException;

import static java.lang.Thread.State.WAITING;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.fail;

public class EntityLockerImplTest {

    @Rule
    public Timeout globalTimeout = Timeout.seconds(1);

    EntityLocker<Integer> locker;

    @Before
    public void setUp() {
        locker = new EntityLockerImpl<>(true);
    }

    @Test
    public void canExecuteProtectedCodeAsSoonAsEntityLockGetsReleasedByAnotherThread() throws InterruptedException, TimeoutException {
        int entityId = 1;

        runWithNewThread(() -> locker.executeLocked(entityId, () -> {
        })).join();

        ensureEntityLocksAreSuccessful(entityId);
        ensureGlobalLocksAreSuccessful();
    }

    @Test
    public void canExecuteProtectedCodeAsSoonAsGlobalLockGetsReleasedByAnotherThread() throws InterruptedException, TimeoutException {
        int entityId = 1;

        runWithNewThread(() -> locker.executeExclusively(() -> {
        })).join();

        ensureEntityLocksAreSuccessful(entityId);
        ensureGlobalLocksAreSuccessful();
    }

    private void ensureGlobalLocksAreSuccessful() throws InterruptedException, TimeoutException {
        locker.executeExclusively(() -> {
        });
        locker.executeExclusively(() -> null);
        locker.tryExecuteExclusively(() -> {
        }, 1, MILLISECONDS);
        locker.tryExecuteExclusively(() -> null, 1, MILLISECONDS);
    }

    private void ensureEntityLocksAreSuccessful(int entityId) throws InterruptedException, TimeoutException {
        locker.executeLocked(entityId, () -> {
        });
        locker.executeLocked(entityId, () -> null);
        locker.tryExecuteLocked(entityId, () -> {
        }, 1, MILLISECONDS);
        locker.tryExecuteLocked(entityId, () -> null, 1, MILLISECONDS);
    }

    @Test
    public void entityLockIsReentrant() {
        locker.executeLocked(1, () -> locker.executeLocked(1, () -> {
        }));
        locker.executeLocked(1, () -> locker.executeLocked(1, () -> null));
        locker.executeLocked(1, () -> {
            try {
                locker.tryExecuteLocked(1, () -> {
                }, 1, MILLISECONDS);
            } catch (InterruptedException | TimeoutException e) {
                failTestWithUnexpectedException(e);
            }
        });
        locker.executeLocked(1, () -> {
            try {
                locker.tryExecuteLocked(1, () -> null, 1, MILLISECONDS);
            } catch (InterruptedException | TimeoutException e) {
                failTestWithUnexpectedException(e);
            }
        });
    }

    @Test
    public void globalLockIsReentrant() {
        locker.executeExclusively(() -> locker.executeExclusively(() -> {
        }));
        locker.executeExclusively(() -> locker.executeExclusively(() -> null));
        locker.executeExclusively(() -> {
            try {
                locker.tryExecuteExclusively(() -> {
                }, 1, MILLISECONDS);
            } catch (InterruptedException | TimeoutException e) {
                failTestWithUnexpectedException(e);
            }
        });
        locker.executeExclusively(() -> {
            try {
                locker.tryExecuteExclusively(() -> null, 1, MILLISECONDS);
            } catch (InterruptedException | TimeoutException e) {
                failTestWithUnexpectedException(e);
            }
        });
    }

    @Test
    public void notAllowedToAcquireEntityLockWhileHoldingGlobalLock() {
        assertThrows(IllegalStateException.class, () -> locker.executeExclusively(() -> locker.executeLocked(1, () -> {
        })));
        assertThrows(IllegalStateException.class, () -> locker.executeExclusively(() -> locker.executeLocked(1, () -> null)));
        assertThrows(IllegalStateException.class, () -> locker.executeExclusively(() -> {
            try {
                locker.tryExecuteLocked(1, () -> {
                }, 1, MILLISECONDS);
            } catch (InterruptedException | TimeoutException e) {
                failTestWithUnexpectedException(e);
            }
        }));
        assertThrows(IllegalStateException.class, () -> locker.executeExclusively(() -> {
            try {
                locker.tryExecuteLocked(1, () -> null, 1, MILLISECONDS);
            } catch (InterruptedException | TimeoutException e) {
                failTestWithUnexpectedException(e);
            }
        }));
    }

    @Test
    public void notAllowedToAcquireGlobalLockWhileHoldingEntityLock() {
        assertThrows(IllegalStateException.class, () -> locker.executeLocked(1, () -> locker.executeExclusively(() -> {
        })));
        assertThrows(IllegalStateException.class, () -> locker.executeLocked(1, () -> locker.executeExclusively(() -> null)));
        assertThrows(IllegalStateException.class, () -> locker.executeLocked(1, () -> {
            try {
                locker.tryExecuteExclusively(() -> {
                }, 1, MILLISECONDS);
            } catch (InterruptedException | TimeoutException e) {
                failTestWithUnexpectedException(e);
            }
        }));
        assertThrows(IllegalStateException.class, () -> locker.executeLocked(1, () -> {
            try {
                locker.tryExecuteExclusively(() -> null, 1, MILLISECONDS);
            } catch (InterruptedException | TimeoutException e) {
                failTestWithUnexpectedException(e);
            }
        }));
    }

    @Test
    public void codeProtectedByGlobalLockCanNotBeExecutedUntilAnyEntityIsLockedByOtherThread() throws InterruptedException, TimeoutException {
        var executionOnGlobalLockHasStarted = new Waiter();
        runWithNewThread(() -> locker.executeLocked(1, () -> {
            executionOnGlobalLockHasStarted.resume();
            try {
                new Waiter().await();
            } catch (InterruptedException | TimeoutException e) {
                executionOnGlobalLockHasStarted.rethrow(e);
            }
        }));
        executionOnGlobalLockHasStarted.await();

        assertThrows(TimeoutException.class, () -> locker.tryExecuteExclusively(() -> {
            Assert.fail("This code should not be executed");
        }, 10, MILLISECONDS));
        assertThrows(TimeoutException.class, () -> locker.tryExecuteExclusively(() -> {
            Assert.fail("This code should not be executed");
            return null;
        }, 10, MILLISECONDS));
    }

    @Test
    public void codeProtectedByGlobalLockCanNotBeExecutedWhileGlobalLockIsLockedByOtherThread() throws InterruptedException, TimeoutException {
        var executionOnGlobalLockHasStarted = new Waiter();
        runWithNewThread(() -> locker.executeExclusively(() -> {
            executionOnGlobalLockHasStarted.resume();
            try {
                new Waiter().await();
            } catch (InterruptedException | TimeoutException e) {
                executionOnGlobalLockHasStarted.rethrow(e);
            }
        }));
        executionOnGlobalLockHasStarted.await();

        assertThrows(TimeoutException.class, () -> locker.tryExecuteExclusively(() -> {
            Assert.fail("This code should not be executed");
        }, 10, MILLISECONDS));
        assertThrows(TimeoutException.class, () -> locker.tryExecuteExclusively(() -> {
            Assert.fail("This code should not be executed");
            return null;
        }, 10, MILLISECONDS));
    }

    @Test
    public void protectedCodeCanExecuteInParallelForDifferentEntityIds() throws InterruptedException {
        var thrower = new Waiter();
        var barrier = new CyclicBarrier(4);
        var t1 = runWithNewThread(() -> locker.executeLocked(1, () -> {
            try {
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                thrower.rethrow(e);
            }
        }));
        var t2 = runWithNewThread(() -> locker.executeLocked(2, () -> {
            try {
                barrier.await();
                return null;
            } catch (InterruptedException | BrokenBarrierException e) {
                thrower.rethrow(e);
                return null;
            }
        }));
        var t3 = runWithNewThread(() -> locker.tryExecuteLocked(3, () -> {
            try {
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                thrower.rethrow(e);
            }
        }, 1, MILLISECONDS), thrower);
        var t4 = runWithNewThread(() -> locker.tryExecuteLocked(4, () -> {
            try {
                barrier.await();
                return null;
            } catch (InterruptedException | BrokenBarrierException e) {
                thrower.rethrow(e);
                return null;
            }
        }, 1, MILLISECONDS), thrower);

        t1.join();
        t2.join();
        t3.join();
        t4.join();
    }

    @Test
    public void atMostOneThreadCanExecuteProtectedCodeForGivenEntity() throws TimeoutException, InterruptedException {
        var entityId = 1;
        var executionOnEntityLockHasStarted = new Waiter();
        runWithNewThread(() -> locker.executeLocked(entityId, () -> {
            try {
                executionOnEntityLockHasStarted.resume();
                new Waiter().await();
            } catch (InterruptedException | TimeoutException e) {
                executionOnEntityLockHasStarted.rethrow(e);
            }
        }));
        executionOnEntityLockHasStarted.await();

        assertThrows(TimeoutException.class, () -> locker.tryExecuteLocked(entityId, () -> {
            fail("This code should not be executed");
        }, 10, MILLISECONDS));
        assertThrows(TimeoutException.class, () -> locker.tryExecuteLocked(entityId, () -> {
            fail("This code should not be executed");
            return null;
        }, 10, MILLISECONDS));
    }

    @Test
    public void theDeadLockIsExpectedIfUserDidNotTakeCareToPreventIt() throws BrokenBarrierException, InterruptedException {
        var waiter = new Waiter();
        CyclicBarrier barrier = new CyclicBarrier(3);
        var t1 = runWithNewThread(() -> locker.executeLocked(1, () -> {
            try {
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                waiter.rethrow(e);
            }
            locker.executeLocked(2, () -> {
            });
        }));

        var t2 = runWithNewThread(() -> locker.executeLocked(2, () -> {
            try {
                barrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                waiter.rethrow(e);
            }
            locker.executeLocked(1, () -> {
            });
        }));
        barrier.await();
        await().atLeast(100, MILLISECONDS)
                .until(() -> t1.getState().equals(WAITING) && t2.getState().equals(WAITING));
    }

    private void failTestWithUnexpectedException(Exception e) {
        e.printStackTrace();
        Assert.fail("This code should not be reached");
    }

    static Thread runWithNewThread(Runnable r) {
        Thread thread = new Thread(r);
        thread.start();
        return thread;
    }

    static Thread runWithNewThread(ThrowingRunnable r, Waiter w) {
        Thread thread = new Thread(() -> {
            try {
                r.run();
            } catch (Throwable throwable) {
                throwable.printStackTrace();
                w.rethrow(throwable);
            }
        });
        thread.start();
        return thread;
    }

}